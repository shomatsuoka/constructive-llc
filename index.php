<!DOCTYPE html>
<html lang="ja">
  <head>
    <?php include 'includes/head.php' ?>
  </head>
  <body class="backdrop">
    <div id="page-animate">

      <div id="main-wrap">
        <?php include 'includes/opening.php'; ?>
        <?php include 'includes/mission.php'; ?>
        <?php include 'includes/order.php' ?>
    		<?php include 'includes/company.php' ?>
        <?php include 'includes/footer.php' ?>
      </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js" integrity="sha512-bLT0Qm9VnAYZDflyKcBaQ2gg0hSYNQrJ8RilYldYQ1FxQYoCLtUjuuRuZo+fjqhx/qtq/1itJ0C2ejDxltZVFg==" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/fetch-jsonp@1.1.3/build/fetch-jsonp.min.js"></script>
    <script src="js/opening.js"></script>
    <script src="js/order.js"></script>
    <script src="js/page-animate.js"></script>
    <!-- <script src="js/validation.js"></script> -->
  </body>

</html>