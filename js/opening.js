setViewHeight("#opening-view")

window.addEventListener("resize", function() {
  setViewHeight("#opening-view");
  setViewHeight("#mission");
})

loader();

setTimeout(function() {
  document.querySelector("#opening-view").classList.remove("displaynone");
}, 100);

function start () {
  // ローディングが終わったら
  document.querySelector("body").classList.remove("backdrop");
  document.querySelector("#opening-view").classList.add("displaynone");
  document.querySelector("#main-wrap").classList.remove("displaynone");

  setViewHeight("#mission");

  document.querySelector("#mission").classList.remove("displaynone");

  if (document.referrer.indexOf(location.host) == -1) {
    // アニメーションあり
    textAnimation();
  } else {
    // アニメーション無し
    setTimeout(function() {
      document.querySelector("#service-txt").classList.add("drop");
      document.querySelector("#first-scroll-btn").classList.add("fadeIn");
      document.querySelector("#page-footer").classList.remove("displaynone");
    }, 1000)

    document.querySelector("#terms-of-service").classList.remove("displaynone");
    document.querySelector("#company").classList.remove("displaynone");
  }
}

function loader () {
  var progressBar = document.querySelector('.js-loader-progress-bar');
  var progressNumber = document.querySelector('.js-loader-progress-number');
  var imgLoad = imagesLoaded('body'); //body内の画像を監視
  var imgTotal = imgLoad.images.length; //画像の総数を取得
  var imgLoaded = 0; //ロードした画像カウント
  var progressSpeed = 1; //プログレスバーの進むスピード 大きいほど速くなる
  var progressCount = 0; //ローディングの進捗状況 0〜100
  var progressResult = 0; //ローディングの進捗状況 0〜100

  //読み込み状況をアップデート
  var progressInit = setInterval(function() {
    updateProgress();
  }, 25);

  //画像を一つロードするたびにimgLoadedのカウントをアップ
  imgLoad.on('progress', function(instance, image) {
    imgLoaded++;
  })

  //読み込み状況アップデート関数
  function updateProgress () {
    //ローディング演出用の計算
    progressCount += (imgLoaded / imgTotal) * progressSpeed;
    if (progressCount >= 100 && imgTotal > imgLoaded) {
      //カウントは100以上だが画像の読み込みが終わってない
      progressResult = 99;
    } else if (progressCount > 99.9) {
      //カウントが100以上になった
      progressResult = 100;
    } else {
      //現在のカウント
      progressResult = progressCount;
    }

    //ローディング進捗状況をプログレスバーと数字で表示
    progressBar.style.width = progressResult + '%';
    // progressNumber.style.left = progressResult + '%';
    // progressNumber.innerText = Math.floor(progressResult);

    //ローディング完了後 0.8秒待ってページを表示
    if (progressResult >= 100 && imgTotal == imgLoaded) {
      clearInterval(progressInit);
      setTimeout(function() {
        var setIntervalId = setInterval(function() {
          if (document.readyState == "complete") {
            clearInterval(setIntervalId);
            document.querySelector('body').classList.add('is-loaded');
            start();
          }
        }, 1)
      }, 800);
    }
  }
}

function setViewHeight(sel) {
  var viewHeight = window.innerHeight;
  document.querySelector(sel).setAttribute("style",
    "height:" + viewHeight + "px;"
  );
}

function textAnimation() {
  var bottomArrowIcon = document.querySelector("i.bottom-arrow");
  var serviceTxt = document.querySelector("#mission #service-txt");
  var h3el = document.querySelector("#mission h3");
  var underlay = document.querySelector("body .underlay");
  var slogan = document.querySelector("#mission .slogan");
  var h2el = document.querySelector("h2#mission-txt");
  var txtarr = h2el.innerText.split('')
  var txt = "";
  var windowHeight = window.innerHeight;
  var termsOfServiceEl = document.querySelector("#terms-of-service");
  var missionContainer = document.querySelector("#mission.container");
  var pageFooter = document.querySelector("#page-footer");
  var companyEl = document.querySelector("#company");

  for (var i = 0; i < txtarr.length; i++) {
    if (txtarr[i] == " ") {
      txtarr[i] = "&nbsp;"
    }
  }

  h2el.innerHTML = '';
  h2el.classList.remove("displaynone");

  for (var i = 0; i < txtarr.length; i++) {
    txt = txt + '<span class="letter">' + txtarr[i] + '</span>';
  }

  h2el.innerHTML = txt;

  addClass(slogan, "let-text-go-top", 0);
  removeClass(underlay, "displaynone", 800);
  removeClass(h3el, "displaynone", 800);
  addClass(h3el, "displaynone", 4000);
  removeClass(serviceTxt, "displaynone", 4000);
  addClass(serviceTxt, "drop", 4000);

  if (windowHeight > 414) {
    removeClass(bottomArrowIcon, "displaynone", 4000);
    addClass(bottomArrowIcon, "fadeIn", 5000);
  }

  removeClass(termsOfServiceEl, "displaynone", 4000);
  removeClass(companyEl, "displaynone", 4000);
  removeClass(pageFooter, "displaynone", 4000);

  addClass(serviceTxt, "drop", 4000);
  addClass(missionContainer, "top-border", 4000);
  addClass(underlay, "displaynone", 4000);

}

/*
*
* addClassをするファンクション
* @function
* @param {node} node
* @param {DOMstring} className - クラス名を書きます。
* @param {Number} milliseconds - 遅延時間をミリ秒で書きます。
*
*/
function addClass(node, className, milliseconds) {
  setTimeout(function() {
    node.classList.add(className);
  }, milliseconds)
}

/*
*
* removeClassをするファンクション
* @function
* @param {node} node
* @param {DOMstring} className - クラス名を書きます。
* @param {Number} milliseconds - 遅延時間をミリ秒で書きます。
*
*/
function removeClass(node, className, milliseconds) {
  setTimeout(function() {
    node.classList.remove(className);
  }, milliseconds)
}