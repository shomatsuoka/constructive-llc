var scrollYEl = document.querySelector("#terms-of-service .scroll-y");

window.addEventListener("scroll", function() {
    setTermsHeight();
});

window.addEventListener("resize", function() {
    setTermsHeight();
});

window.addEventListener("orientationchange", function() {
  setTimeout(setTermsHeight, 400);
});



setTermsHeight();

function setTermsHeight() {
	var innerHeight = window.innerHeight;
  var termsHeight = innerHeight - 300;
  // console.log(innerHeight)
  if (innerHeight >= 414) {
  	scrollYEl.setAttribute("style", "height:" + termsHeight + "px");
  }

}

