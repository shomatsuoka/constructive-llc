// Scroll
var termsOfServiceEl = document.querySelector("#terms-of-service");

// 下矢印ボタン
var firstScrollBtn = document.querySelector("#first-scroll-btn");
var viewHeight = window.innerHeight;

if (firstScrollBtn) {
  firstScrollBtn.addEventListener("click", function() {
    var pageHeaderEl = document.querySelector("#page-header");
    var headerHeight = pageHeaderEl.clientHeight;
    // var scrollHeight = viewHeight - headerHeight;
    $("html, body").stop().animate({ scrollTop: viewHeight - headerHeight }, 500, 'swing');
  });
}

window.addEventListener("scroll", function() {
  var termsOfServiceElPos = termsOfServiceEl.getBoundingClientRect().top;
  var pageHeaderEl = document.querySelector("#page-header");
  var headerHeight = pageHeaderEl.clientHeight;
  var scrollPos = window.pageYOffset;

  if (termsOfServiceElPos <= headerHeight + 200) {
    pageHeaderEl.classList.add("drop");
  } else {
    pageHeaderEl.classList.remove("drop");
  }

  if (termsOfServiceElPos < 200 && firstScrollBtn && viewHeight > 414) {
    firstScrollBtn.classList.remove("fadeIn");
    setTimeout(function() {
      firstScrollBtn.classList.add("displaynone;");
    }, 200);
  }

  if (scrollPos == 0 && firstScrollBtn && viewHeight > 414) {
    firstScrollBtn.classList.remove("displaynone;");
    setTimeout(function() {
      firstScrollBtn.classList.add("fadeIn");
    }, 200);
  }

});

// Selectbox
var dropdownEl = document.querySelector(".dropdown");
var menuEl = document.querySelector(".menu");
dropdownEl.addEventListener("click", function() {
  menuEl.classList.toggle("showMenu");
  rotateArrow();
});

var menuListEls = document.querySelectorAll(".menu li");
for (var i = 0; i < menuListEls.length; i++) {
  menuListEls[i].addEventListener("click", function(e) {
    var dropdownPEl = document.querySelector(".dropdown p");
    dropdownPEl.innerHTML = e.currentTarget.innerHTML;
    menuEl.classList.remove("showMenu");
    rotateArrow();
    setDropDownValue();
  });
}

function rotateArrow() {
  var dropdownIEl = document.querySelector(".dropdown i");
  if (menuEl.classList.contains("showMenu")) {
    dropdownIEl.classList.add("rotateArrow");
  } else {
    dropdownIEl.classList.remove("rotateArrow");
  }
}

function setDropDownValue() {
  var dropdownTxt = document.querySelector(".dropdown p").innerText;
  var hiddenInputEl = document.querySelector(".dropdown input");
  hiddenInputEl.setAttribute("value", dropdownTxt);
}

// Postcode
// var search = document.querySelector("#form .search-address-btn");
// search.addEventListener('click', function(event) {
//   event.preventDefault();
//   var api = 'https://zipcloud.ibsnet.co.jp/api/search?zipcode=';
//   var input = document.getElementById('input-postcode');
//   var address = document.getElementById('input-address');
//   var param = input.value.replace("-", ""); //入力された郵便番号から「-」を削除
//   var url = api + param;
//   var error = document.querySelector("#form #input-postcode-wrap .error-message");

//   fetchJsonp(url, {
//       timeout: 10000, //タイムアウト時間
//     })
//     .then(function(response) {
//       return response.json();
//     })
//     .then(function(data) {
//       if (data.status === 400) { // エラー時
//         console.log(data.message);
//         error.innerHTML = "住所が見つかりませんでした。";
//         // search.setAttribute("style", "background: #535faa");
//       } else if (data.results === null) {
//         error.innerHTML = "住所が見つかりませんでした。";
//         // search.setAttribute("style", "background: #535faa");
//       } else {
//         error.innerHTML = "";
//         // search.setAttribute("style", "background: #221815");
//         address.value = data.results[0].address1 +
//           data.results[0].address2 +
//           data.results[0].address3;
//       }
//     })
//     .catch(function(ex) { //例外処理
//       console.log(ex);
//     });
// }, false);

// WEBストレージ
var inputCorporateType = document.querySelector(".dropdown input");
var inputCorporateName = document.querySelector("#input-corporate-name");
var inputYourName = document.querySelector("#input-your-name");
var inputPostcode = document.querySelector("#input-postcode");
var inputAddress = document.querySelector("#input-address");
var inputPhoneNumber = document.querySelector("#input-phone-number");
var inputEmailAddress = document.querySelector("#input-email-address");
var inputMessage = document.querySelector("#input-message");
var largeReadTermsBtn = document.querySelector("#read-terms-btn");
var termesOfServicePageSendBtn = document.querySelector("#terms-of-service #send-btn");
var dropdownEl = document.querySelector("#form .dropdown");

largeReadTermsBtn.addEventListener("click", function() {
  createData();
});

termesOfServicePageSendBtn.addEventListener("click", function() {
  createData();
});

var localStorageData = JSON.parse(localStorage.getItem("ConstructiveLLC_Send_Form_Data"));
var dropdownPEl = document.querySelector(".dropdown p");

if (localStorageData) {
  inputCorporateType.value = localStorageData.corporateType;
  dropdownPEl.innerHTML = localStorageData.corporateType;
  inputCorporateName.value = localStorageData.corporateName;
  inputYourName.value = localStorageData.nameOfPersonInCharge;
  inputPostcode.value = localStorageData.postCode;
  inputAddress.value = localStorageData.address;
  inputPhoneNumber.value = localStorageData.phoneNumber;
  inputEmailAddress.value = localStorageData.emailAddress;
  inputMessage.value = localStorageData.message;
}

function createData() {
  var inputData = {
    corporateType: inputCorporateType.value,
    corporateName: inputCorporateName.value,
    nameOfPersonInCharge: inputYourName.value,
    postCode: inputPostcode.value,
    address: inputAddress.value,
    phoneNumber: inputPhoneNumber.value,
    emailAddress: inputEmailAddress.value,
    message: inputMessage.value
  };

  localStorage.setItem("ConstructiveLLC_Send_Form_Data", JSON.stringify(inputData));

}
