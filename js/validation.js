// VALIDATION ここから
var inputCorporateType = document.querySelector(".dropdown input");
var inputCorporateName = document.querySelector("#input-corporate-name");
var inputYourName = document.querySelector("#input-your-name");
var inputPostcode = document.querySelector("#input-postcode");
var inputAddress = document.querySelector("#input-address");
var inputPhoneNumber = document.querySelector("#input-phone-number");
var inputEmailAddress = document.querySelector("#input-email-address");
var inputMessage = document.querySelector("#input-message");
var largeReadTermsBtn = document.querySelector("#read-terms-btn");
var termesOfServicePageSendBtn = document.querySelector("#terms-of-service #send-btn");
var dropdownEl = document.querySelector("#form .dropdown");

var termesOfServicePageSendBtn = document.querySelector("#terms-of-service #send-btn");
var dropdownPEl = document.querySelector(".dropdown p");
var selectCorporateTypeErrEl = document.querySelector("#select-corporate-type-wrap .error-message");
var inputCorporateNameErrEl = document.querySelector("#input-corporate-name-wrap .error-message");
var inputYourNameErrEl = document.querySelector("#input-your-name-wrap .error-message");
var inputPostcodeErrEl = document.querySelector("#input-postcode-wrap .error-message");
var inputAddressErrEl = document.querySelector("#input-address-wrap .error-message");
var inputPhoneNumberErrEl = document.querySelector("#input-phone-number-wrap .error-message");
var inputEmailAddressErrEl = document.querySelector("#input-email-address-wrap .error-message");
var inputMessageErrEl = document.querySelector("#input-message-wrap .error-message");

termesOfServicePageSendBtn.addEventListener("click", function(e) {
  selectCorporateTypeErr(e);
  inputCorporateNameErr(e);
  inputYourNameErr(e);
  inputPostcodeErr(e);
  inputAddressErr(e);
  inputPhoneNumberErr(e);
  inputEmailAddressErr(e);
  inputMessageErr(e);
});

dropdownEl.addEventListener("click", function() {
  selectCorporateTypeErr();
})

inputCorporateName.addEventListener("blur", function() {
  inputCorporateNameErr();
})

inputYourName.addEventListener("blur", function() {
  inputYourNameErr();
})

inputPostcode.addEventListener("blur", function() {
  inputPostcodeErr();
})

inputAddress.addEventListener("blur", function() {
  inputAddressErr();
})

inputPhoneNumber.addEventListener("blur", function() {
  inputPhoneNumberErr();
})

inputEmailAddress.addEventListener("blur", function() {
  inputEmailAddressErr();
})

inputMessage.addEventListener("blur", function() {
  inputMessageErr();
})

function selectCorporateTypeErr(e) {
  if (dropdownPEl.innerText == "いずれかをお選びください" || dropdownPEl.innerText == "") {
    console.log("法人種別を入力");
    insertErrorMessage(e, selectCorporateTypeErrEl, "必須")
  }
}

function inputCorporateNameErr(e) {
  if (!inputCorporateName.value) {
    console.log("法人名を入力");
    insertErrorMessage(e, inputCorporateNameErrEl, "必須")
  }
}

function inputYourNameErr(e) {
  if (!inputYourName.value) {
    console.log("担当者名を入力");
    insertErrorMessage(e, inputYourNameErrEl, "必須")
  }
}

function inputPostcodeErr(e) {
  if (!inputPostcode.value) {
    console.log("郵便番号を入力");
    insertErrorMessage(e, inputPostcodeErrEl, "必須")
  }
}

function inputAddressErr(e) {
  if (!inputAddress.value) {
    console.log("住所を入力");
    insertErrorMessage(e, inputAddressErrEl, "必須")
  }
}

function inputPhoneNumberErr(e) {
  if (!inputPhoneNumber.value) {
    console.log("電話番号を入力");
    insertErrorMessage(e, inputPhoneNumberErrEl, "必須")
  }
}

function inputEmailAddressErr(e) {
  if (!inputEmailAddress.value) {
    console.log("メールアドレスを入力");
    insertErrorMessage(e, inputEmailAddressErrEl, "必須")
  }
}

function inputMessageErr(e) {
  if (!inputMessage.value) {
    console.log("お問合せ内容を入力");
    insertErrorMessage(e, inputMessageErrEl, "必須")
  }
}

function insertErrorMessage(e, errorMessageElement, message) {
  errorMessageElement.innerText = message;
  if (e) {
    e.preventDefault();
  }
}

//VALIDATION ここまで