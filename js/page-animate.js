// 画面が読み込まれた時、is-slideを外し、アニメーションさせる
var wrapEl = document.querySelector("#page-animate");
var nowLoading = document.querySelector("#nowloading");

window.onload = function() {
	wrapEl.classList.remove("is-slide");
	if (nowLoading) {
		nowLoading.classList.add("displaynone")
	}
}

pageTransition("#read-terms-btn", "terms-page.php");
pageTransition("#write-order-form-btn", "order-page.php")
pageTransition("#back-btn", "order-page.php")
pageTransition("#page-header h1 img.logo-en", "index.php")

function pageTransition(selector, uri) {
	var el = document.querySelector(selector)
	if(el) {
		el.addEventListener("click", function(e) {
	  	wrapEl.classList.add("is-slide-in");
	  	setTimeout(function() {
				location.href = uri
	  	}, 700)
		})
	}
}