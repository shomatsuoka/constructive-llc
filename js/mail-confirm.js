// ご確認画面（mail.php）
var sendBtn = document.querySelector("#formWrap #send-btn");
if (sendBtn) {
  sendBtn.addEventListener("click", function() {
    var localStorageData = JSON.parse(localStorage.getItem("ConstructiveLLC_Send_Form_Data"));

    var inputData = {
      corporateType: localStorageData.corporateType,
      corporateName: localStorageData.corporateName,
      nameOfPersonInCharge: localStorageData.nameOfPersonInCharge,
      postCode: localStorageData.postCode,
      address: localStorageData.address,
      phoneNumber: localStorageData.phoneNumber,
      emailAddress: localStorageData.emailAddress,
      message: ""
    }

    localStorage.removeItem("ConstructiveLLC_Send_Form_Data");
    localStorage.setItem("ConstructiveLLC_Send_Form_Data", JSON.stringify(inputData));

  })
}