<section class="container big-logo displaynone" id="opening-view">
  <h1 class="white-logo">
    <img src="svg/logo-en-white.svg" alt="CONSTRUCTIVE LLC">
    <aside>
      <img src="svg/logo-jp-white.svg" alt="コンストラクティブ合同会社">
    </aside>
  </h1>
  <div class="js-loader">
    <div class="js-loader-progress">
      <div class="js-loader-progress-bar"></div>
        <!--
          <div class="js-loader-progress-number"></div>
        -->
    </div>
  </div>
</section>

