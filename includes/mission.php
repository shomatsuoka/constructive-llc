    <section class="container displaynone" id="mission">
    	<div class="underlay displaynone"></div>
      <article class="slogan">
        <h2 id="mission-txt" class="displaynone">
          <!-- IMPROVING UX QUALITY -->
        </h2>
      </article>
      <h3 class="displaynone">CREATE A CULTURE OF EXCELLENCE</h3>

      <article id="service-txt">
      	<h4>JavaScript and UI/UX <span>for the Modern Web Development</span></h4>
				<aside>JavaScriptのWEB開発・UI/UX設計のご用命は<span>コンストラクティブ合同会社まで</span></aside>
      </article>

      <i class="bottom-arrow" id="first-scroll-btn"></i>
    </section>