<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0">
  <meta name="description" content="JavaScriptのWEB開発・UI/UX設計のご用命はコンストラクティブ合同会社まで">
  <meta name="twitter:card" content="summary">
  <meta name="twitter:site" content="@ESDOT_FREIT">
  <meta property="og:title" content="CONSTRUCTIVE LLC | コンストラクティブ合同会社">
  <meta property="og:type" content="company">
  <meta property="og:url" content="https://constructive.co.jp">
  <meta property="og:region" content="東京">
  <meta property="og:country-name" content="日本">
  <meta property="og:email" content="info@constructive.co.jp">
  <meta property="og:phone_number" content="+81-50-3649-4722">
  <meta property="og:image" content="http://constructive.jp/ogp/ogp.png">
  <meta property="og:site_name" content="CONSTRUCTIVE LLC | コンストラクティブ合同会社">
  <meta property="og:description" content="JavaScriptのWEB開発・UI/UX設計のご用命はコンストラクティブ合同会社まで">
  <meta name="twitter:card" content="summary_large_image">
  <meta name="twitter:title" content="CONSTRUCTIVE LLC | コンストラクティブ合同会社">
  <meta name="twitter:description" content="JavaScriptのWEB開発・UI/UX設計のご用命はコンストラクティブ合同会社まで">
  <title>CONSTRUCTIVE LLC | コンストラクティブ合同会社</title>

  <link rel="shortcut icon" href="favicon/favicon.ico" >
  <link rel="stylesheet" href="css/common.css">
  <link rel="stylesheet" href="css/opening-view.css">
  <link rel="stylesheet" href="css/terms-of-service.css">
  <link rel="stylesheet" href="css/order.css">
  <link rel="stylesheet" href="css/mission.css">
  <link rel="stylesheet" href="css/company.css">
  <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
  <script src="https://kit.fontawesome.com/3513518fcb.js" crossorigin="anonymous"></script>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-GC1ENN424E"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', 'G-GC1ENN424E');
	</script>
</head>
