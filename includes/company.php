<article id="company" class="displaynone">
	<div class="container cf">
		<div class="hr"><img src="png/diamond.png" alt="" class="diamond"></div>
		<section class="company-detail">
			<h3>
				<img src="png/forest.png" alt="">
				会社概要
			</h3>
			<table>
				<tbody>
					<tr>
						<th>称号</th>
						<td>コンストラクティブ合同会社</td>
					</tr>
					<tr>
						<th>設立</th>
						<td>2018年（平成30年）10月2日</td>
					</tr>
					<tr>
						<th>住所</th>
						<td>〒104-0061 東京都中央区銀座一丁目22番11号 銀座大竹ビジデンス2F</td>
					</tr>
					<tr>
						<th>E-MAIL</th>
						<td>info@constructive.co.jp</td>
					</tr>
					<tr>
						<th>電話番号</th>
						<td>050-3649-4722</td>
					</tr>
					<tr>
						<th>代表者</th>
						<td>松岡将</td>
					</tr>
					<tr>
						<th>事業内容</th>
						<td>WEB開発、UI設計、データベース設計、広告の作成、音楽プロダクションの製作</td>
					</tr>
				</tbody>
			</table>
		</section>
	</div>
</article>