<article id="terms-of-service" class="displaynone">
  <?php include 'includes/header.php'; ?>
  <div class="container cf">
    <section class="order-form">
      <h3>ご依頼・お問合せ</h3>
      <form action="mail.php" method="post" id="form" name="order">
        <label for="select-corporate-type" id="select-corporate-type-wrap">
          法人種別
          <div class="dropholder">
            <div class="dropdown">
              <input id="input-corporate-type" type="hidden" name="法人種別" value="" visibility="hidden">
              <p>いずれかをお選びください</p>
              <i><img src="png/down-arrow.png"></i>
            </div>
            <ul class="menu">
              <li>株式会社</li>
              <li>有限会社</li>
              <li>合同会社</li>
              <li>社団法人</li>
              <li>学校法人</li>
              <li>合資会社</li>
              <li>その他の法人（個人事業主以外）</li>
            </ul>
          </div>
          <aside class="notice">
            法人専用サービスとなりますため個人事業主を含む個人の方はご利用いただけません。
          </aside>
          <span class="error-message att"></span>
        </label>

        <label for="input-corporate-name" id="input-corporate-name-wrap">
          法人名
          <input type="text" id="input-corporate-name" name="法人名">
          <span class="error-message att"></span>
        </label>

        <label for="input-your-name" id="input-your-name-wrap">
          ご担当者名
          <input type="text" id="input-your-name" name="ご担当社名">
          <span class="error-message att"></span>
        </label>

        <label for="input-postcode" id="input-postcode-wrap">
<!--           <button class="search-address-btn btn">住所を検索</button> -->
          郵便番号
          <input type="text" id="input-postcode" name="郵便番号">
          <span class="error-message att"></span>
        </label>

        <label for="input-address" id="input-address-wrap">
          住所
          <input type="text" id="input-address" name="住所">
          <span class="error-message att"></span>
        </label>

        <label for="input-phone-number" id="input-phone-number-wrap">
          電話番号
          <input type="text" id="input-phone-number" name="電話番号">
          <span class="error-message att"></span>
        </label>

        <label for="input-email-address" id="input-email-address-wrap">
          Eメール
          <input type="text" id="input-email-address" name="Eメール">
          <span class="error-message att"></span>
        </label>

        <label for="input-message" id="input-message-wrap">
          お問合せ内容
          <textarea id="input-message" name="お問い合わせ内容"></textarea>
          <span class="error-message att"></span>
        </label>

        <footer>
            <button type="button" id="read-terms-btn" class="btn">
              サービス利用規約を読む
            </button>

            <aside class="notice">
              弊社の<span class="att" id="read-terms">サービス利用規約</span>にご同意いただいた方のみ、本サービスをご利用いただけます。
            </aside>

            <button id="send-btn" type="submit" class="btn">利用規約に同意をして<span class="att">送信する</span></button>

          <aside class="tel">
            お急ぎの場合は<span>お電話を</span>
            <div id="tel-wrap">
              <a href="tel:05036494722">TEL: 050-3649-4722</a>
              <div class="bar"></div>
            </div>
          </aside>
        </footer>
      </form>
    </section>
  </div>
</article>